# Makiwara

Makiwara is a monospaced serif typeface based on the original [Computer Modern Metafont source files](https://www.ctan.org/pkg/cm-mf) by Donald E. Knuth. A normal and *italic* cut have been rendered using Metafont with custom parameter settings and some slight changes to the Computer Modern source files. 

<img src="by-products/dish_4-36pt.png" width="320">

## Outline (vector) font files

Outline versions of those two cuts have been traced using [mftrace](https://github.com/hanwen/mftrace) and are available as TrueType fonts in the [outline](/outline) directory.

## Rendering and customizations

To render your own versions of Makiwara — Computer Modern actually —, clone this repository, customize your parameter settings in `mwm12.mf` (normal) or `mwmi12.mf` (*italic*) and render using Metafont.

```bash
$ git clone --recurse-submodules git@gitlab.com:swrs/makiwara.git 
```

This clones the Makiwara repository including the [mfbuild](https://gitlab.com/swrs/mfbuild) submodule. mfbuild is a set of handy tools facilitating the preview and tracing of meta-fonts. If you want to omit mfbuild, omit `--recurse-submodules`.

Using mfbuild you can view and trace font files. For more information, see the documentation of the [mfbuild repo](https://gitlab.com/swrs/mfbuild).

```bash
$ cd makiwara
$ mfbuild/view mwm12.mf # view characters of Makiwara normal
$ mfbuild/trace mwm12.mf Makiwara-Custom Normal ./output # trace a "Normal" cut of the family "Makiwara-Custom" and save makiwara-custom-normal.pfa font file in ./output
```
