% This is the file for additional characters specific to Makiwara. Some of them are a
% selection of characters present in Computer Modern's source files.

cmchar "Hyphen";
beginchar("-",6u#,x_height#,0);
italcorr .5x_height#*slant-.5u#;
adjust_fit(0,0);
numeric thickness; thickness=if hefty:bar else:.75[hair,stem] fi;
pickup crisp.nib; pos1(thickness,90); pos2(thickness,90);
top y1r=top y2r=vround(.5h+.5thickness); rt x2=hround(w-u)+eps;
if monospace: x2=w-x1 else: lft x1=hround .2u-eps fi;
filldraw stroke z1e--z2e;  % bar
penlabels(1,2); endchar;

cmchar "Opening quotes";
beginchar(oct"134",7u#+max(2u#,dot_size#),asc_height#,0);
italcorr asc_height#*slant-.1u#;
adjust_fit(0,0);
x2+.5dot_size=hround(w-.6u); y1+.5dot_size=h-comma_depth;
x2-x1=hround(1.5u+max(2u,dot_size)); y2=y1;
ammoc(1,a,dot_size,.25u,comma_depth); % left dot and tail
ammoc(2,b,dot_size,.25u,comma_depth); % right dot and tail
penlabels(1,2); endchar;

cmchar "Closing quotes";
beginchar(oct"042",7u#+max(2u#,dot_size#),asc_height#,0);
italcorr asc_height#*slant+dot_size#-4.1u#;
adjust_fit(0,0);
x1-.5dot_size=hround .6u; y2+.5dot_size=h;
x2-x1=hround(1.5u+max(2u,dot_size)); y2=y1;
comma(1,a,dot_size,.25u,comma_depth); % left dot and tail
comma(2,b,dot_size,.25u,comma_depth); % right dot and tail
penlabels(1,2); endchar;

cmchar "En dash";
beginchar(oct"173",9u#,x_height#,0);
italcorr .61803x_height#*slant+.5u#;
adjust_fit(0,0);
pickup crisp.nib; pos1(vair,90); pos2(vair,90);
top y1r=top y2r=vround(.61803h+.5vair); lft x1=-eps; rt x2=w+eps;
filldraw stroke z1e--z2e;  % bar
penlabels(1,2); endchar;

cmchar "Em dash";
beginchar(oct"174",18u#,x_height#,0);
italcorr .61803x_height#*slant+.5u#;
adjust_fit(letter_fit#,letter_fit#);
pickup crisp.nib; pos1(vair,90); pos2(vair,90);
top y1r=top y2r=vround(.61803h+.5vair); lft x1=-eps; rt x2=w+eps;
filldraw stroke z1e--z2e;  % bar
penlabels(1,2); endchar;

cmchar "Sterling sign";
beginchar("$",12u#,asc_height#,0);
adjust_fit(0,.75asc_height#*slant-.5u#); pickup fine.nib;
pos0(flare,0); pos1(hair,0); pos2(vair,90); pos3(stem,180);
pos4(stem,180); pos4'(stem,0); pos5(vair,-90); z4'=z4;
x2=2/3w-.5u; rt x3l=rt x4l=hround(.5w-u+.5stem); x5=2.5u;
y1=y3=.75h; top y2=h+oo; y4=.25h; bot y5r=-oo;
rt x1r=hround(w-1.5u); bulb(2,1,0);  % bulb
filldraw stroke pulled_arc.e(2,3)..z4e;  % stem
numeric light_stem; light_stem=2/3[vair,vstem];
pos6(.5[hair,light_stem],-180); pos7(light_stem,-300);
pos8(light_stem,-300); pos9(hair,-180);
lft x6r=hround u; x7=3u; x8=w-3.5u; rt x9l=hround(w-u);
y6=.4[y5,y7]; top y7r=vround .2h; bot y8l=-oo; y9=good.y .2h;
filldraw stroke pulled_arc.e(4',5)...z6e{up}...z7e{right}
 ..{right}z8e...{up}z9e;  % loop and arm
pos10(bar,90); pos11(bar,90); x10=3u; x11=w-4.5u;
top y10r=top y11r=vround(.5h+.5bar); filldraw stroke z10e--z11e;  % bar
penlabels(1,2,3,4,5,6,7,8,9,10,11); endchar;

cmchar "Guillemot left";
compute_spread(5/4x_height#,3/2x_height#);
beginchar("<",14u#,v_center(spread#+rule_thickness#));
italcorr h#*slant-u#;
adjust_fit(0,0); pickup rule.nib;
lft x2=hround u-eps; x1=x3=w/2+.5u;
y1-y3=spread; y2=.5[y1,y3]=math_axis;
x4=x6=w/2+x1-2u; x5=w/2+x2-2u;
y4=y1; y5=y2; y6=y3;
draw z1--z2--z3;  % diagonals
draw z4--z5--z6;  % diagonals
labels(1,2,3,4,5,6); endchar;

cmchar "Guillemot right";
compute_spread(5/4x_height#,3/2x_height#);
beginchar(">",14u#,v_center(spread#+rule_thickness#));
italcorr h#*slant-u#;
adjust_fit(0,0); pickup rule.nib;
lft x1=hround u-eps; x3=x1; x2=w/2+.5u;
y1-y3=spread; y2=.5[y1,y3]=math_axis;
x4=x6=w/2+x1-2u; x5=w/2+x2-2u;
y4=y1; y5=y2; y6=y3;
draw z1--z2--z3;  % diagonals
draw z4--z5--z6;  % diagonals
labels(1,2,3,4,5,6); endchar;
